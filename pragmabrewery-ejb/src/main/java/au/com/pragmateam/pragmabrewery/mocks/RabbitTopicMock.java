package au.com.pragmateam.pragmabrewery.mocks;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

import au.com.pragmateam.pragmabrewery.dto.BeerDTO;

@Named
@Startup
@Singleton
@ApplicationScoped
public class RabbitTopicMock {

	@Inject
	Event<BeerDTO> beerEvent;

	public void send(BeerDTO beer) {
		beerEvent.fire(beer);
	}

}
