package au.com.pragmateam.pragmabrewery.mocks;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import au.com.pragmateam.pragmabrewery.entity.Beer;

@Named
@Startup
@Singleton
@ApplicationScoped
@Lock(LockType.WRITE)
public class BeerDatabaseMock {

	private static volatile ConcurrentMap<Long, Beer> beerEntities = new ConcurrentHashMap<Long, Beer>();

	public void saveOrUpdate(Beer beer) {
		if (beer.getId() == null) {
			beer.setId(new Random().nextLong());
		}
		beerEntities.put(beer.getId(), beer);
	}

	public void delete(Long id) {
		if (id == null) {
			throw new IllegalArgumentException("Beer Id can not be null");
		}
		beerEntities.remove(id);
	}

	public List<Beer> listAll() {
		return beerEntities.values().stream().collect(Collectors.toList());
	}

	public Beer findById(Long id) {
		if (id == null) {
			throw new IllegalArgumentException("Beer Id can not be null");
		}
		return beerEntities.get(id);
	}

	public static void reset() {
		beerEntities.clear();
	}

}
