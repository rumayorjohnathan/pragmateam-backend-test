package au.com.pragmateam.pragmabrewery.entity;

import java.io.Serializable;

import au.com.pragmateam.pragmabrewery.dto.BeerDTO;

public class Beer implements Serializable {

	private static final long serialVersionUID = 7904852403057451823L;

	private Long id;
	private String variety;
	private String description;
	private Integer minTemperature;
	private Integer maxTemperature;
	private Float currentTemperature;

	public Beer() {
		super();
	}

	public Beer(Long id, String variety, String description, Integer minTemperature, Integer maxTemperature,
			Float currentTemperature) {
		this();
		this.id = id;
		this.variety = variety;
		this.description = description;
		this.minTemperature = minTemperature;
		this.maxTemperature = maxTemperature;
		this.currentTemperature = currentTemperature;
	}

	public Beer(BeerDTO beer) {
		this();
		this.id = beer.getId();
		this.variety = beer.getVariety();
		this.description = beer.getDescription();
		this.minTemperature = beer.getMinTemperature();
		this.maxTemperature = beer.getMaxTemperature();
		this.currentTemperature = beer.getCurrentTemperature();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVariety() {
		return variety;
	}

	public void setVariety(String variety) {
		this.variety = variety;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getMinTemperature() {
		return minTemperature;
	}

	public void setMinTemperature(Integer minTemperature) {
		this.minTemperature = minTemperature;
	}

	public Integer getMaxTemperature() {
		return maxTemperature;
	}

	public void setMaxTemperature(Integer maxTemperature) {
		this.maxTemperature = maxTemperature;
	}

	public Float getCurrentTemperature() {
		return currentTemperature;
	}

	public void setCurrentTemperature(Float currentTemperature) {
		this.currentTemperature = currentTemperature;
	}

	@Override
	public String toString() {
		return "Beer [id=" + id + ", variety=" + variety + ", description=" + description + ", minTemperature="
				+ minTemperature + ", maxTemperature=" + maxTemperature + ", currentTemperature=" + currentTemperature
				+ "]";
	}

}
