package au.com.pragmateam.pragmabrewery.messaging;

import javax.ejb.Local;
import javax.enterprise.event.Observes;

import au.com.pragmateam.pragmabrewery.dto.BeerDTO;

@Local
public interface IRabbitConsumerMock {

	void readMessage(@Observes BeerDTO beerDto);

}
