package au.com.pragmateam.pragmabrewery.interceptor;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.pragmateam.pragmabrewery.business.BeerBusiness;
import au.com.pragmateam.pragmabrewery.dto.BeerDTO;
import au.com.pragmateam.pragmabrewery.messaging.IRabbitProducerMock;
import au.com.pragmateam.pragmabrewery.util.BeerBiding;

@Interceptor
@BeerBiding
public class BeerChangeInterceptor {

	@Inject
	private IRabbitProducerMock rabbitProducer;

	private static final Logger LOG = LoggerFactory.getLogger(BeerChangeInterceptor.class);

	@AroundInvoke
	public Object intercept(InvocationContext context) throws Exception {
		if (context.getTarget() instanceof BeerBusiness && context.getParameters()[0] instanceof BeerDTO) {
			final BeerDTO beer = (BeerDTO) context.getParameters()[0];
			if (beer.isCurrentTemperatureOutOfRange()) {
				LOG.info(StringUtils.join("sending message for beer out of normal temperature: ", beer.toString()));
				rabbitProducer.sendMessage(beer);
			}
		}
		return context.proceed();
	}
}
