package au.com.pragmateam.pragmabrewery.dto;

import java.beans.Transient;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import au.com.pragmateam.pragmabrewery.entity.Beer;
@JsonIgnoreProperties(ignoreUnknown = true)
public class BeerDTO implements Serializable {

	private static final long serialVersionUID = 6412721975453190515L;

	private Long id;
	private String variety;
	private String description;
	private Integer minTemperature;
	private Integer maxTemperature;
	private Float currentTemperature;

	public BeerDTO() {
		super();
	}

	public BeerDTO(Long id, String variety, String description, Integer minTemperature, Integer maxTemperature,
			Float currentTemperature) {
		this();
		this.id = id;
		this.variety = variety;
		this.description = description;
		this.minTemperature = minTemperature;
		this.maxTemperature = maxTemperature;
		this.currentTemperature = currentTemperature;
	}

	public BeerDTO(Beer beer) {
		this();
		this.id = beer.getId();
		this.variety = beer.getVariety();
		this.description = beer.getDescription();
		this.minTemperature = beer.getMinTemperature();
		this.maxTemperature = beer.getMaxTemperature();
		this.currentTemperature = beer.getCurrentTemperature();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVariety() {
		return variety;
	}

	public void setVariety(String variety) {
		this.variety = variety;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getMinTemperature() {
		return minTemperature;
	}

	public void setMinTemperature(Integer minTemperature) {
		this.minTemperature = minTemperature;
	}

	public Integer getMaxTemperature() {
		return maxTemperature;
	}

	public void setMaxTemperature(Integer maxTemperature) {
		this.maxTemperature = maxTemperature;
	}

	public Float getCurrentTemperature() {
		return currentTemperature;
	}

	public void setCurrentTemperature(Float currentTemperature) {
		this.currentTemperature = currentTemperature;
	}

	@Transient
	public Boolean isCurrentTemperatureOutOfRange() {
		return currentTemperature > Float.valueOf(this.maxTemperature) || currentTemperature < Float.valueOf(this.minTemperature);
	}

	@Override
	public String toString() {
		return "BeerDTO [id=" + id + ", variety=" + variety + ", description=" + description + ", minTemperature="
				+ minTemperature + ", maxTemperature=" + maxTemperature + ", currentTemperature=" + currentTemperature
				+ "]";
	}

}
