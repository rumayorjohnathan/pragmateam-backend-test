package au.com.pragmateam.pragmabrewery.business;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.interceptor.Interceptors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.pragmateam.pragmabrewery.dto.BeerDTO;
import au.com.pragmateam.pragmabrewery.entity.Beer;
import au.com.pragmateam.pragmabrewery.interceptor.BeerChangeInterceptor;
import au.com.pragmateam.pragmabrewery.repository.IBeerRepository;
import au.com.pragmateam.pragmabrewery.util.PragmaBreweryException;

@Named
@Stateless
public class BeerBusiness implements IBeerBusiness {

	@Inject
	private IBeerRepository beerRepository;

	private static final Logger LOG = LoggerFactory.getLogger(BeerBusiness.class);

	@Override
	public BeerDTO findById(Long id) throws PragmaBreweryException {
		LOG.info(StringUtils.join("finding by id -> ", id));
		final Beer beer = verifyIntegrity(id);
		return new BeerDTO(beer);
	}

	@Override
	public List<BeerDTO> listAll() {
		LOG.info("listing all");
		return beerRepository.listAll().stream().map(b -> new BeerDTO(b)).collect(Collectors.toList());
	}

	@Override
	@Interceptors({ BeerChangeInterceptor.class })
	public void save(BeerDTO beer) throws PragmaBreweryException {
		LOG.info(StringUtils.join("saving beer -> ", beer.toString()));
		if (beer.getId() != null) {
			throw new PragmaBreweryException("Conflict, id already registered.", 409);
		}
		beerRepository.save(new Beer(beer));
	}

	@Override
	@Interceptors({ BeerChangeInterceptor.class })
	public void update(Long id, BeerDTO beer) throws PragmaBreweryException {
		LOG.info(StringUtils.join("updating beer -> ", beer.toString()));
		verifyIntegrity(id);
		beerRepository.update(id, new Beer(beer));
	}

	@Override
	public void delete(Long id) throws PragmaBreweryException {
		LOG.info(StringUtils.join("deleting by id -> ", id));
		verifyIntegrity(id);
		beerRepository.delete(id);
	}
	
	private Beer verifyIntegrity(Long id) throws PragmaBreweryException {
		final Beer beer = beerRepository.findById(id);
		if (beer == null) {
			throw new PragmaBreweryException("Beer not found", 404);
		}
		return beer;
	}

}
