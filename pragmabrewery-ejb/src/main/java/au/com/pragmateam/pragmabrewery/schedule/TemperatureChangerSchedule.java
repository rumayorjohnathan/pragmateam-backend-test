package au.com.pragmateam.pragmabrewery.schedule;

import java.util.Random;

import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.pragmateam.pragmabrewery.business.IBeerBusiness;
import au.com.pragmateam.pragmabrewery.dto.BeerDTO;
import au.com.pragmateam.pragmabrewery.util.PragmaBreweryException;

@Named
@Stateless
public class TemperatureChangerSchedule {

	@Inject
	private IBeerBusiness beerBusiness;

	private static final Logger LOG = LoggerFactory.getLogger(TemperatureChangerSchedule.class);

	private static final Random RANDOM = new Random();

	@Schedule(dayOfMonth = "*", hour = "*", minute = "*/1", second = "00", persistent = false)
	public void changeTemperature() throws PragmaBreweryException {
		LOG.info("initializing temperature changing.");
		final BeerDTO beer = beerBusiness.findById(Long.valueOf(RANDOM.nextInt(5)+1));
		LOG.info(StringUtils.join("changing temperature for beer -> ", beer.toString()));
		beer.setCurrentTemperature(Float.valueOf(RANDOM.nextInt(9)) + Float.valueOf(RANDOM.nextInt(9) / 10F));
		LOG.info(StringUtils.join("changed temperature for beer -> ", beer.toString()));
		beerBusiness.update(beer.getId(), beer);
	}
}
