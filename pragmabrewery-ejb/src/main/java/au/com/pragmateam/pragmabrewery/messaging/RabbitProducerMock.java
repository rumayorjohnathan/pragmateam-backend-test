package au.com.pragmateam.pragmabrewery.messaging;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.pragmateam.pragmabrewery.dto.BeerDTO;
import au.com.pragmateam.pragmabrewery.mocks.RabbitTopicMock;

@Named
@Stateless
public class RabbitProducerMock implements IRabbitProducerMock {

	@Inject
	private RabbitTopicMock rabbitTopic;

	private static final Logger LOG = LoggerFactory.getLogger(RabbitProducerMock.class);

	@Override
	public void sendMessage(BeerDTO beer) {
		LOG.info(StringUtils.join("sending beer to alert topic: ", beer.toString()));
		rabbitTopic.send(beer);
	}

}
