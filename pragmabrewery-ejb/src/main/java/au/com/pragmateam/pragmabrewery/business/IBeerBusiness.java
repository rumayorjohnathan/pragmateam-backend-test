package au.com.pragmateam.pragmabrewery.business;

import java.util.List;

import javax.ejb.Local;

import au.com.pragmateam.pragmabrewery.dto.BeerDTO;
import au.com.pragmateam.pragmabrewery.util.PragmaBreweryException;

@Local
public interface IBeerBusiness {

	BeerDTO findById(Long id) throws PragmaBreweryException;

	List<BeerDTO> listAll();

	void save(BeerDTO beer) throws PragmaBreweryException;

	void update(Long id, BeerDTO beer) throws PragmaBreweryException;

	void delete(Long id) throws PragmaBreweryException;

}
