package au.com.pragmateam.pragmabrewery.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.pragmateam.pragmabrewery.entity.Beer;
import au.com.pragmateam.pragmabrewery.mocks.BeerDatabaseMock;

@Named
@Stateless
public class BeerRepository implements IBeerRepository {

	@Inject
	private BeerDatabaseMock beerDatabase;

	private static final Logger LOG = LoggerFactory.getLogger(BeerRepository.class);

	@Override
	public Beer findById(Long id) {
		LOG.info(StringUtils.join("finding by id -> ", id));
		return beerDatabase.findById(id);
	}

	@Override
	public List<Beer> listAll() {
		LOG.info("listing all");
		return beerDatabase.listAll();
	}

	@Override
	public void save(Beer beer) {
		LOG.info(StringUtils.join("saving beer -> ", beer.toString()));
		beerDatabase.saveOrUpdate(beer);
	}
	
	@Override
	public void update(Long id, Beer beer) {
		LOG.info(StringUtils.join("updating beer -> ", beer.toString()));
		beerDatabase.saveOrUpdate(beer);
	}

	@Override
	public void delete(Long id) {
		LOG.info(StringUtils.join("deleting by id -> ", id));
		beerDatabase.delete(id);
	}

}
