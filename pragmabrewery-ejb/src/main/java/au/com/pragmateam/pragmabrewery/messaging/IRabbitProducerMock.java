package au.com.pragmateam.pragmabrewery.messaging;

import javax.ejb.Local;

import au.com.pragmateam.pragmabrewery.dto.BeerDTO;

@Local
public interface IRabbitProducerMock {

	void sendMessage(BeerDTO beerDto);

}
