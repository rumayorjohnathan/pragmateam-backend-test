package au.com.pragmateam.pragmabrewery.repository;

import java.util.List;

import javax.ejb.Local;

import au.com.pragmateam.pragmabrewery.entity.Beer;

@Local
public interface IBeerRepository {

	Beer findById(Long id);

	List<Beer> listAll();

	void save(Beer beer);

	void update(Long id, Beer beer);

	void delete(Long id);

}
