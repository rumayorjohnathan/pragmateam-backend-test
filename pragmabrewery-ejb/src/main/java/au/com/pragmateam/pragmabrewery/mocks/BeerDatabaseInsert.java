package au.com.pragmateam.pragmabrewery.mocks;

import javax.annotation.PostConstruct;
import javax.ejb.DependsOn;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import au.com.pragmateam.pragmabrewery.entity.Beer;

@Startup
@ApplicationScoped
@Singleton
@DependsOn("BeerDatabaseMock")
public class BeerDatabaseInsert {

	@Inject
	private BeerDatabaseMock beerDatabase;

	@PostConstruct
	public void insertBeers() {
		beerDatabase.saveOrUpdate(new Beer(1L, "Pilsner", "", 4, 6, 5F));
		beerDatabase.saveOrUpdate(new Beer(2L, "Ipa", "", 5, 6, 5.5F));
		beerDatabase.saveOrUpdate(new Beer(3L, "Lager", "", 4, 7, 6F));
		beerDatabase.saveOrUpdate(new Beer(4L, "Stout", "", 6, 8, 7F));
		beerDatabase.saveOrUpdate(new Beer(5L, "Wheat Beer", "", 3, 5, 4F));
		beerDatabase.saveOrUpdate(new Beer(6L, "PaleAle", "", 4, 6, 5F));
	}
	
	public void reset() {
		BeerDatabaseMock.reset();
		insertBeers();
	}
}
