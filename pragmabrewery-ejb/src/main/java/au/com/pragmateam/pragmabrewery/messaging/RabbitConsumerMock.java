package au.com.pragmateam.pragmabrewery.messaging;

import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.pragmateam.pragmabrewery.dto.BeerDTO;

@Named
@Stateless
public class RabbitConsumerMock implements IRabbitConsumerMock {

	private static final Logger LOG = LoggerFactory.getLogger(RabbitConsumerMock.class);

	@Override
	public void readMessage(@Observes BeerDTO beerDto) {
		LOG.info(StringUtils.join("reading message from topic: ", beerDto.toString()));
	}

}
