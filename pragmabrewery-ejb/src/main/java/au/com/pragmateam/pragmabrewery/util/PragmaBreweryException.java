package au.com.pragmateam.pragmabrewery.util;

public class PragmaBreweryException extends Exception {

	private static final long serialVersionUID = 2363385333569559840L;

	private Integer code;

	public PragmaBreweryException(String message, Integer code) {
		super(message);
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

}
