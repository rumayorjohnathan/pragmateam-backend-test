package au.com.pragmateam.pragmabrewery.business;
//

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import au.com.pragmateam.pragmabrewery.dto.BeerDTO;
import au.com.pragmateam.pragmabrewery.mocks.BeerDatabaseInsert;
import au.com.pragmateam.pragmabrewery.util.PragmaBreweryException;

@RunWith(Arquillian.class)
public class BeerBusinessTest {

	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap.create(WebArchive.class, "test.war")
				.addPackage("*")
				.addPackages(true, Filters.exclude(".*Test.*"), "")
				.addClass(StringUtils.class)
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource("test-ds.xml", "test-ds.xml");
	}

	@Inject
	private IBeerBusiness beerBusiness;

	@Inject
	private BeerDatabaseInsert databaseInsert;

	@Before
	public void setUp() {
		databaseInsert.reset();
	}

	@Test
	public void findByIdTestOk() throws Exception {
		final Long id = 4L;
		final BeerDTO beer = beerBusiness.findById(id);
		assertEquals(id, beer.getId());
		assertNotNull(beer);
	}

	@Test
	public void findByIdTestNotFound() throws Exception {
		final Long id = 10L;
		try {
			beerBusiness.findById(id);
		} catch (PragmaBreweryException pbe) {
			final Integer code = 404;
			assertEquals(pbe.getClass(), PragmaBreweryException.class);
			assertEquals(pbe.getCode(), code);
		}
	}

	@Test
	public void listAllTestOk() throws Exception {
		final List<BeerDTO> beers = beerBusiness.listAll();
		assertEquals(beers.size(), 6);
	}

	@Test
	public void saveTestOk() throws Exception {
		final BeerDTO beer = new BeerDTO(null, "Brahma", "", 3, 8, 6.5F);
		beerBusiness.save(beer);
	}

	public void saveTestConflict() throws Exception {
		try {
			beerBusiness.save(new BeerDTO(1L, "Brahma", "", 3, 8, 6.5F));
		} catch (PragmaBreweryException pbe) {
			final Integer code = 409;
			assertEquals(pbe.getClass(), PragmaBreweryException.class);
			assertEquals(code, pbe.getCode());
		}
	}

	@Test
	public void updateTestOk() throws Exception {
		final BeerDTO beer = new BeerDTO(1L, "Brahma", "", 3, 8, 6.5F);
		beerBusiness.update(1L, beer);
		final List<BeerDTO> beers = beerBusiness.listAll();
		assertEquals(beers.size(), 6);
	}

	@Test
	public void updateTestNotFound() throws Exception {
		try {
			beerBusiness.update(10L, new BeerDTO(10L, "Brahma", "", 3, 8, 6.5F));
		} catch (PragmaBreweryException pbe) {
			final Integer code = 404;
			assertEquals(pbe.getClass(), PragmaBreweryException.class);
			assertEquals(code, pbe.getCode());
		}
	}

	@Test
	public void deleteTestOk() throws Exception {
		beerBusiness.delete(1L);
		final Integer size = 5;
		final Integer currentSize = beerBusiness.listAll().size();
		assertEquals(size, currentSize);
	}

	@Test
	public void deleteTestNotFound() throws Exception {
		try {
			beerBusiness.delete(10L);
		} catch (PragmaBreweryException pbe) {
			final Integer code = 404;
			assertEquals(pbe.getClass(), PragmaBreweryException.class);
			assertEquals(code, pbe.getCode());
		}
	}
}
