package au.com.pragmateam.pragmabrewery.repository;
//

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import au.com.pragmateam.pragmabrewery.entity.Beer;
import au.com.pragmateam.pragmabrewery.mocks.BeerDatabaseInsert;

@RunWith(Arquillian.class)
public class BeerRepositoryTest {

	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap.create(WebArchive.class, "test.war")
				.addPackage("*")
				.addPackages(true, Filters.exclude(".*Test.*"), "")
				.addClass(StringUtils.class)
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource("test-ds.xml", "test-ds.xml");
	}

	@Inject
	private BeerDatabaseInsert databaseInsert;

	@Before
	public void setUp() {
		databaseInsert.reset();
	}

	@Inject
	private IBeerRepository beerRepository;

	@Test
	public void findByIdTestOk() throws Exception {
		final Long id = 4L;
		final Beer beer = beerRepository.findById(id);
		assertEquals(id, beer.getId());
		assertNotNull(beer);
	}

	@Test
	public void findByIdTestNotFound() throws Exception {
		final Long id = 10L;
		assertNull(beerRepository.findById(id));
	}

	@Test
	public void listAllTestOk() throws Exception {
		final List<Beer> beers = beerRepository.listAll();
		assertEquals(beers.size(), 6);
	}

	@Test
	public void saveTestOk() throws Exception {
		final Beer beer = new Beer(null, "Brahma", "", 3, 8, 6.5F);
		beerRepository.save(beer);
		final List<Beer> beers = beerRepository.listAll();
		assertEquals(beers.size(), 7);
	}

	@Test
	public void updateTestOk() throws Exception {
		final Beer beer = new Beer(1L, "Brahma", "", 3, 8, 6.5F);
		beerRepository.update(1L, beer);
		final List<Beer> beers = beerRepository.listAll();
		assertEquals(beers.size(), 6);
	}

	@Test
	public void updateTestNotFound() throws Exception {
		beerRepository.update(10L, new Beer(10L, "Brahma", "", 3, 8, 6.5F));
		final List<Beer> beers = beerRepository.listAll();
		assertEquals(beers.size(), 7);
	}

	@Test
	public void deleteTestOk() throws Exception {
		beerRepository.delete(1L);
		final Integer size = 5;
		final Integer currentSize = beerRepository.listAll().size();
		assertEquals(size, currentSize);
	}

	@Test
	public void deleteTestNotFound() throws Exception {
		beerRepository.delete(10L);
		final List<Beer> beers = beerRepository.listAll();
		assertEquals(beers.size(), 6);
	}
}
