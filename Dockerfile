FROM jboss/wildfly
 
ADD pragmabrewery-ear/target/pragmabrewery-ear.ear /opt/jboss/wildfly/standalone/deployments/

EXPOSE 8080

LABEL maintainer="rumayorjohnathan@gmail.com"
