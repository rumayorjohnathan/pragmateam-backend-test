package au.com.pragmateam.pragmabrewery.rest;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.pragmateam.pragmabrewery.business.IBeerBusiness;
import au.com.pragmateam.pragmabrewery.dto.BeerDTO;
import au.com.pragmateam.pragmabrewery.util.PragmaBreweryException;

@Path("/beers")
@RequestScoped
public class BeerApi {

	private static final Logger LOG = LoggerFactory.getLogger(BeerApi.class);

	@Inject
	private IBeerBusiness beerBusiness;

	@GET
	@Path(StringUtils.EMPTY)
	@Produces(MediaType.APPLICATION_JSON)
	public Response listAll() {
		LOG.info("listing all");
		return Response.ok(beerBusiness.listAll()).build();
	}

	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findById(@PathParam("id") Long id) {
		try {
			LOG.info(StringUtils.join("searching for beer id: ", id));
			return Response.ok(beerBusiness.findById(id)).build();
		} catch (PragmaBreweryException e) {
			return Response.status(e.getCode()).build();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@POST
	@Path(StringUtils.EMPTY)
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createBeer(BeerDTO beer) {
		try {
			LOG.info(StringUtils.join("saving beer: ", beer.toString()));
			beerBusiness.save(beer);
			return Response.status(Status.CREATED).build();
		} catch (PragmaBreweryException e) {
			return Response.status(e.getCode()).build();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@PUT
	@Path("/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateBeer(@PathParam("id") Long id, BeerDTO beer) {
		try {
			LOG.info(StringUtils.join("updating beer: ", beer.toString()));
			beerBusiness.update(id, beer);
			return Response.ok().build();
		} catch (PragmaBreweryException e) {
			return Response.status(e.getCode()).build();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@DELETE
	@Path("/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteBeer(@PathParam("id") Long id) {
		try {
			LOG.info(StringUtils.join("deleting beer for id: ", id));
			beerBusiness.delete(id);
			return Response.status(Status.NO_CONTENT).build();
		} catch (PragmaBreweryException e) {
			return Response.status(e.getCode()).build();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
}
