package au.com.pragmateam.pragmabrewery.rest;

import static org.junit.Assert.assertEquals;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.extension.rest.client.ArquillianResteasyResource;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import au.com.pragmateam.pragmabrewery.dto.BeerDTO;

@RunWith(Arquillian.class)
public class BeerApiTest {

	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap.create(WebArchive.class, "pragmabrewery-web.war")
				.addPackage("*")
				.addPackages(true, Filters.exclude(".*Test.*"), "")
				.addClass(StringUtils.class)
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Test
	@RunAsClient
	public void listAllTestOk(@ArquillianResteasyResource final WebTarget webTarget) {
		final Response response = webTarget
				.path("/beers")
				.request(MediaType.APPLICATION_JSON)
				.get();
		assertEquals(200, response.getStatus());
		assertEquals(BeerDTO[].class, response.readEntity(BeerDTO[].class).getClass());
	}

	@Test
	@RunAsClient
	public void findByIdTestOk(@ArquillianResteasyResource final WebTarget webTarget) {
		final Long id = 1L;
		final Response response = webTarget
				.path("/beers/1")
				.request(MediaType.APPLICATION_JSON)
				.get();
		assertEquals(200, response.getStatus());
		final BeerDTO beer = response.readEntity(BeerDTO.class);
		assertEquals(id, beer.getId());
	}

	@Test
	@RunAsClient
	public void findByIdTestNotFound(@ArquillianResteasyResource final WebTarget webTarget) {
		final Response response = webTarget
				.path("/beers/10")
				.request(MediaType.APPLICATION_JSON)
				.get();
		assertEquals(404, response.getStatus());
	}
	
	@Test
	@RunAsClient
	public void createBeerTestOk(@ArquillianResteasyResource final WebTarget webTarget) {
		final Response response = webTarget
				.path("/beers")
				.request(MediaType.APPLICATION_JSON)
				.post(Entity.json(new BeerDTO(null, "Test", "", 4, 10, 7.5F)));
		assertEquals(201, response.getStatus());
	}
	
	@Test
	@RunAsClient
	public void createBeerTestConflict(@ArquillianResteasyResource final WebTarget webTarget) {
		final Response response = webTarget
				.path("/beers")
				.request(MediaType.APPLICATION_JSON)
				.post(Entity.json(new BeerDTO(10L, "Test", "", 4, 10, 7.5F)));
		assertEquals(409, response.getStatus());
	}

	@Test
	@RunAsClient
	public void updateBeerTestOk(@ArquillianResteasyResource final WebTarget webTarget) {
		final Response response = webTarget
				.path("/beers/1")
				.request(MediaType.APPLICATION_JSON)
				.put(Entity.json(new BeerDTO(null, "Test", "", 4, 10, 7.5F)));
		assertEquals(200, response.getStatus());
	}

	@Test
	@RunAsClient
	public void updateBeerTestNotFound(@ArquillianResteasyResource final WebTarget webTarget) {
		final Response response = webTarget
				.path("/beers/10")
				.request(MediaType.APPLICATION_JSON)
				.put(Entity.json(new BeerDTO(10L, "Test", "", 4, 10, 7.5F)));
		assertEquals(404, response.getStatus());
	}

	@Test
	@RunAsClient
	public void deleteBeerTestOk(@ArquillianResteasyResource final WebTarget webTarget) {
		final Response response = webTarget
				.path("/beers/2")
				.request(MediaType.APPLICATION_JSON)
				.delete();
		assertEquals(204, response.getStatus());
	}

	@Test
	@RunAsClient
	public void deleteBeerTestNotFound(@ArquillianResteasyResource final WebTarget webTarget) {
		final Response response = webTarget
				.path("/beers/14")
				.request(MediaType.APPLICATION_JSON)
				.delete();
		assertEquals(404, response.getStatus());
	}
}
